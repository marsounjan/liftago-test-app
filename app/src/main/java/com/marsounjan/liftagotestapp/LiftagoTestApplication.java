package com.marsounjan.liftagotestapp;

import android.app.Application;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 06.09.16.
 */
public class LiftagoTestApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        RemoteLiftagoServiceManager.init(this);
    }
}
