package com.marsounjan.liftagotestapp;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 06.09.16.
 */
public class RemoteLiftagoServiceManager {

    private final String LIFTAGO_SERVICE_ACTION_ID = "com.adleritech.android.developertest.SimulatorService";
    final int MSG_OUT_REGISTER = 1;
    final int MSG_OUT_ON_BROADCAST = 2;
    final int MSG_OUT_GET_STATE = 3;

    final int MSG_IN_OK = 999;
    final int MSG_IN_ERROR = 1000;
    final int MSG_IN_ON_RIDE_START = 1001;
    final int MSG_IN_ON_RIDE_FINISH = 1002;
    final int MSG_IN_STATE = 1003;

    static final int STATE_WAITING = 1;
    static final int STATE_BROADCASTING = 2;
    static final int STATE_RIDE = 3;

    public static RemoteLiftagoServiceManager INSTANCE;
    private Context context;
    private LiftagoServiceState state;
    private int serviceManagerRefCount;
    private boolean serviceBound;
    private Messenger serviceMessanger;
    private Messenger receiveMessenger;
    private List<RemoteLiftagoServiceManagerStateChangeListener> stateChangeListeners;

    public static void init(Application application){
        if(INSTANCE == null){
            INSTANCE = new RemoteLiftagoServiceManager(application);
        }
    }

    private RemoteLiftagoServiceManager(Context context) {
        this.context = context;
        this.serviceBound = false;
        this.stateChangeListeners = new ArrayList<>();
        this.serviceManagerRefCount = 0;
    }

    public void increaseRefCount() {
        if (serviceManagerRefCount == 0 || !serviceBound){
            bindLiftagoService();
        }
        serviceManagerRefCount++;
    }

    public void decreaseRefCount() {
        if (serviceManagerRefCount == 0 || --serviceManagerRefCount == 0) {
            unBindLiftagoService();
        }
    }

    private void bindLiftagoService(){
        Intent serviceIntent = new Intent(LIFTAGO_SERVICE_ACTION_ID);
        serviceIntent.setPackage("com.adleritech.android.developertest");
        context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void unBindLiftagoService(){
        try {
            context.unbindService(serviceConnection);
        } catch (IllegalArgumentException e){
            Log.e(RemoteLiftagoServiceManager.class.getName(),"", e);
        }
        this.serviceBound = false;
        setState(null);
        receiveMessenger = null;
        serviceMessanger = null;
    }

    private void sendMessage_Register() throws RemoteException{
        Message message = Message.obtain();
        message.replyTo = receiveMessenger;
        message.what = MSG_OUT_REGISTER;
        serviceMessanger.send(message);
    }

    public void sendMessage_Broadcast() throws RemoteException{
        Message message = Message.obtain();
        message.replyTo = receiveMessenger;
        message.what = MSG_OUT_ON_BROADCAST;
        try {
            serviceMessanger.send(message);
        } catch(RemoteException e){
            unBindLiftagoService();
            throw e;
        }
    }

    private void sendMessage_GetState(){
        Message message = Message.obtain();
        message.replyTo = receiveMessenger;
        message.what = MSG_OUT_GET_STATE;
        try {
            serviceMessanger.send(message);
        } catch(RemoteException e){
            unBindLiftagoService();
        }
    }

    /** Defines callbacks for service binding, passed to increaseRefCount() */
    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            serviceMessanger = new Messenger(service);
            receiveMessenger = new Messenger(new IncomingHandler());
            serviceBound = true;
            if (serviceManagerRefCount == 0){
                unBindLiftagoService();
            } else {
                try{
                    sendMessage_Register();
                } catch (RemoteException e){
                    unBindLiftagoService();
                }
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            serviceMessanger = null;
            serviceBound = false;
        }
    };

    public LiftagoServiceState getState() {
        return state;
    }

    private void setState(LiftagoServiceState newState){
        if((state != null && !state.equals(newState) || ((newState == null && state != null) || (newState != null && state == null)))){
            state = newState;
            for(RemoteLiftagoServiceManagerStateChangeListener listener : stateChangeListeners){
                listener.onStateChanged(newState);
            }
        }
    }

    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if(serviceBound){
                switch (msg.what) {
                    case MSG_IN_STATE:
                        setState(LiftagoServiceState.fromStateID(msg.arg1));
                        break;
                    case MSG_IN_OK:
                        if(state != null && state.equals(LiftagoServiceState.waiting)){
                            setState(LiftagoServiceState.broadcasting);
                        } else {
                            sendMessage_GetState();
                        }
                        break;
                    case MSG_IN_ERROR:
                        setState(LiftagoServiceState.fromStateID(msg.arg1));
                        break;
                    case MSG_IN_ON_RIDE_START:
                        if(state != null && state.equals(LiftagoServiceState.broadcasting)){
                            setState(LiftagoServiceState.ride);
                        } else {
                            sendMessage_GetState();
                        }
                        break;
                    case MSG_IN_ON_RIDE_FINISH:
                        if(state != null && state.equals(LiftagoServiceState.ride)){
                            setState(LiftagoServiceState.waiting);
                        } else {
                            sendMessage_GetState();
                        }
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        }
    }

    public void registerStateChangeListener(RemoteLiftagoServiceManagerStateChangeListener listener){
        stateChangeListeners.add(listener);
    }

    public void unRegisterStateChangeListener(RemoteLiftagoServiceManagerStateChangeListener listener){
        stateChangeListeners.remove(listener);
    }

    public interface RemoteLiftagoServiceManagerStateChangeListener{
        void onStateChanged(LiftagoServiceState newState);
    }

}
