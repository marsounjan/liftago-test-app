package com.marsounjan.liftagotestapp.view;

import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.marsounjan.liftagotestapp.LiftagoServiceState;
import com.marsounjan.liftagotestapp.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 06.09.16.
 */
public class WaitingActivity extends RemoteServiceBaseActivity{

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_broadcast)
    public void onBroadcastClicked(){
        try {
            remoteLiftagoServiceManager.sendMessage_Broadcast();
        } catch(RemoteException e){
            Toast.makeText(this, "Error! Service not bound correctly!", Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected boolean isAssociatedStateWithThisActivity(LiftagoServiceState newState) {
        return newState != null && newState.equals(LiftagoServiceState.waiting);
    }
}
