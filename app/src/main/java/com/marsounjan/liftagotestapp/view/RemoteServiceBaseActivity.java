package com.marsounjan.liftagotestapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.marsounjan.liftagotestapp.LiftagoServiceState;
import com.marsounjan.liftagotestapp.RemoteLiftagoServiceManager;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 06.09.16.
 */
public abstract class RemoteServiceBaseActivity extends AppCompatActivity implements RemoteLiftagoServiceManager.RemoteLiftagoServiceManagerStateChangeListener {

    protected RemoteLiftagoServiceManager remoteLiftagoServiceManager;

    @Override
    public void onCreate(Bundle savedBundleState) {
        super.onCreate(savedBundleState);
        this.remoteLiftagoServiceManager = RemoteLiftagoServiceManager.INSTANCE;
        this.remoteLiftagoServiceManager.increaseRefCount();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.remoteLiftagoServiceManager.decreaseRefCount();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.remoteLiftagoServiceManager.registerStateChangeListener(this);
        switchActivityBasedOnLiftagoServiceState(remoteLiftagoServiceManager.getState());
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.remoteLiftagoServiceManager.unRegisterStateChangeListener(this);
    }

    @Override
    public void onStateChanged(LiftagoServiceState newState) {
        switchActivityBasedOnLiftagoServiceState(newState);
    }

    private void switchActivityBasedOnLiftagoServiceState(LiftagoServiceState newState){
        if(isAssociatedStateWithThisActivity(newState)){
            //already showing associated activity
            return;
        }
        Intent intent;
        if(newState == null){
            intent = new Intent(this, MainActivity.class);
        } else {
            switch (newState){
                case waiting:
                    intent = new Intent(this, WaitingActivity.class);
                    break;
                case broadcasting:
                    intent = new Intent(this, BroadcastingActivity.class);
                    break;
                case ride:
                    intent = new Intent(this, RideActivity.class);
                    break;
                default:
                    //should never happen
                    return;
            }
        }
        startActivity(intent);
        finish();
    }

    abstract protected boolean isAssociatedStateWithThisActivity(LiftagoServiceState newState);
}
