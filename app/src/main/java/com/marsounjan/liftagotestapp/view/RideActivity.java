package com.marsounjan.liftagotestapp.view;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.marsounjan.liftagotestapp.LiftagoServiceState;
import com.marsounjan.liftagotestapp.R;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 06.09.16.
 */
public class RideActivity extends RemoteServiceBaseActivity{

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride);
    }

    @Override
    protected boolean isAssociatedStateWithThisActivity(LiftagoServiceState newState) {
        return newState != null && newState.equals(LiftagoServiceState.ride);
    }
}
