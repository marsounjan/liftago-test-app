package com.marsounjan.liftagotestapp.view;

import android.os.Bundle;

import com.marsounjan.liftagotestapp.LiftagoServiceState;
import com.marsounjan.liftagotestapp.R;

public class MainActivity extends RemoteServiceBaseActivity {

    @Override
    public void onCreate(Bundle savedBundleState) {
        super.onCreate(savedBundleState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected boolean isAssociatedStateWithThisActivity(LiftagoServiceState newState) {
        return newState == null;
    }
}
