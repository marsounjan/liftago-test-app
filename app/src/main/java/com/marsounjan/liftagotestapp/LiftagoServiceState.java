package com.marsounjan.liftagotestapp;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 06.09.16.
 */
public enum LiftagoServiceState {

    waiting(RemoteLiftagoServiceManager.STATE_WAITING),
    broadcasting(RemoteLiftagoServiceManager.STATE_BROADCASTING),
    ride(RemoteLiftagoServiceManager.STATE_RIDE);

    int stateID;

    LiftagoServiceState(int stateID) {
        this.stateID = stateID;
    }

    public static LiftagoServiceState fromStateID(int stateID){
        switch (stateID){
            case RemoteLiftagoServiceManager.STATE_WAITING:
                return waiting;
            case RemoteLiftagoServiceManager.STATE_BROADCASTING:
                return broadcasting;
            case RemoteLiftagoServiceManager.STATE_RIDE:
                return ride;
            default:
                return null;
        }
    }

}
